/*
 Navicat Premium Data Transfer

 Source Server         : mysql8本地连接
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : localhost:3306
 Source Schema         : vertxblog

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 19/07/2022 23:20:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '内容',
  `classId` int(11) NOT NULL COMMENT '分类id',
  `state` tinyint(4) NOT NULL DEFAULT 1 COMMENT '状态',
  `isHot` tinyint(4) NOT NULL DEFAULT 0 COMMENT '置顶',
  `isRecommend` tinyint(4) NOT NULL DEFAULT 0 COMMENT '推荐',
  `createDate` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `lastModifiedDate` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `likeCount` int(11) DEFAULT 0 COMMENT '点赞数',
  `collectCount` int(11) DEFAULT 0 COMMENT '收藏数',
  `commentCount` int(11) DEFAULT 0 COMMENT '评论数',
  `browseCount` int(11) DEFAULT 0 COMMENT '浏览量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES (1, '测试', '## 测试', 1, 2, 0, 0, '2022-06-04 17:38:14', '2022-06-04 17:38:16', 0, 0, 0, 0);
INSERT INTO `article` VALUES (2, '测试2', '## 测试2', 1, 2, 0, 0, '2022-06-04 17:38:14', '2022-06-04 17:38:16', 0, 0, 0, 0);
INSERT INTO `article` VALUES (4, 'elementary os安装配置', '# elementary os安装后配置\r\n\r\n参考链接  https://www.jianshu.com/p/f0298125ad84/\r\n\r\n*2020年10月25日 更新博客*\r\n\r\n*注意：本人使用最新的 elementaryos-5.1-stable.20200814.iso，貌似安装deepin-wine有问题，导致开机黑屏，大家自行测试。*\r\n\r\n\r\n## 一、换源\r\n不换源！不用换源！不要换源！\r\n\r\n\r\n## 二、安装elementary-tweaks\r\n\r\n```bash\r\nsudo add-apt-repository ppa:philip.scott/elementary-tweaks\r\nsudo apt update\r\nsudo apt install elementary-tweaks\r\n```\r\n\r\n## 三、安装 wingpanel-indicator-ayatana 第三方应用状态栏图标修复\r\n\r\n1. weget下载安装\r\n\r\n```bash\r\nwget http://ppa.launchpad.net/elementary-os/stable/ubuntu/pool/main/w/wingpanel-indicator-ayatana/wingpanel-indicator-ayatana_2.0.3+r27+pkg17~ubuntu0.4.1.1_amd64.deb\r\n\r\nsudo dpkg -i wingpanel-indicator-ayatana_2.0.3+r27+pkg17~ubuntu0.4.1.1_amd64.deb\r\n```\r\n\r\n2. 网盘下载到本地安装\r\n下面提供 deb的安装包，重启\r\n  链接：[https://share.weiyun.com/5ecLBmv](https://share.weiyun.com/5ecLBmv) 密码：htg5kj\r\n\r\n\r\n## 四、安装搜狗输入法\r\n1. 卸载 ibus\r\n\r\n```bash\r\nsudo apt remove ibus  scim\r\nsudo apt autoremove\r\n```\r\n2. 安装 Eddy 软件包安装器\r\n![安装eddy](https://img-blog.csdnimg.cn/20191209215804240.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDIxNDI0Mw==,size_16,color_FFFFFF,t_70)\r\n\r\n3. 安装搜狗输入法\r\n\r\n下载搜狗输入法deb包，链接 https://pinyin.sogou.com/linux/  ，使用 Eddy 安装器安装搜狗输入法deb包（自动安装相关依赖）\r\n\r\n4. 设置默认\r\n\r\n```bash\r\nsudo im-config -s fcitx -z default\r\n```\r\n5. 重启下，或注销重新登录，看看现在应该可以使用了。\r\n\r\n\r\n\r\n## 五、安装字体\r\n建议安装下 **微软雅黑、宋体、新宋体**\r\n![安装字体](https://img-blog.csdnimg.cn/20191209221119910.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDIxNDI0Mw==,size_16,color_FFFFFF,t_70)\r\n\r\n\r\n## 六、安装 desktop folder 桌面管理软件\r\n![desktop folder](https://img-blog.csdnimg.cn/20191209221416529.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDIxNDI0Mw==,size_16,color_FFFFFF,t_70)\r\n安装后运行下，桌面就可以放图标了\r\n\r\n## 七、其他软件安装\r\n安装  360浏览器（链接：https://mirrors.huaweicloud.com/deepin/pool/non-free/b/browser360-cn-stable/ ）、谷歌浏览器、火狐浏览器、网易云音乐（链接：http://d1.music.126.net/dmusic/netease-cloud-music_1.2.1_amd64_ubuntu_20190428.deb ）、QQ、微信、任务管理器等等。\r\nQQ 、 微信、钉钉 可以到  \r\n\r\n> 优麒麟官网 （链接 ：https://www.ubuntukylin.com/applications/）  \r\n\r\n>麒麟软件官网（链接： http://www.kylinos.cn/adaptation/application.html）  .\r\n\r\n> ------另QQ官方原生Linux版已更新只第二版，自行下载安装，链接：https://im.qq.com/linuxqq/download.html  \r\n\r\n 的软件下载区下载，使用 Eddy安装即可。\r\n\r\n\r\n## 八、关于elementary os网易云音乐托盘图标不显示菜单解决方法\r\n转载至：https://www.cnblogs.com/dudujerry/p/10152326.html\r\n\r\n1. 将 `/etc/xdg/autostart/indicator-application.desktop` 中的\r\n\r\n```bash\r\nOnlyShowIn=Unity;GNOME;\r\n```\r\n\r\n修改为\r\n\r\n```bash\r\nOnlyShowIn=Unity;GNOME;Pantheon;\r\n```\r\n\r\n\r\n2. 修改网易云音乐的.desktop\r\n找到网易云音乐的.desktop，编辑（随便选一种吧）：\r\n\r\n```bash\r\nsudo nano /usr/share/applications/netease-cloud-music.desktop\r\nsudo io.elementary.code /usr/share/applications/netease-cloud-music.desktop\r\nsudo gedit /usr/share/applications/netease-cloud-music.desktop\r\n```\r\n\r\n将\r\n\r\n```bash\r\nExec=netease-cloud-music %U\r\n```\r\n\r\n修改为\r\n\r\n```bash\r\nExec=env XDG_CURRENT_DESKTOP=Unity netease-cloud-music %U\r\n```\r\n\r\n保存退出这样就可以用了。\r\n\r\n\r\n\r\n## 九、关于 deepin-wine qq/tim 无法接收图片解决方法\r\nhttps://bbs.deepin.org/forum.php?mod=viewthread&tid=189805&extra=page%3D1\r\n\r\n\r\n## 十、关于独显驱动的安装\r\n一般在应用商店会有独显驱动推荐，安装即可。\r\n应用商店==>更新--> \r\n例如，英伟达的  NVIDIA-drive-390    点击后面安装、免费、...（翻译可能有点问题）', 2, 1, 0, 0, '2022-06-19 22:31:07', '2022-06-19 22:31:10', 0, 0, 0, 0);
INSERT INTO `article` VALUES (7, '测试3', '## 测试3', 1, 2, 0, 0, '2022-06-04 17:38:14', '2022-06-04 17:38:16', 0, 0, 0, 0);
INSERT INTO `article` VALUES (14, '测试添加文章111', '## 测试添加文章111', 4, 1, 0, 0, '2022-07-18 00:02:27', '2022-07-18 00:02:30', 0, 0, 0, 0);
INSERT INTO `article` VALUES (15, '测试编辑文章222', '## 测试编辑文章222', 5, 1, 0, 0, '2022-07-18 00:04:36', '2022-07-18 00:17:01', 0, 0, 0, 0);

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `state` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '1' COMMENT '状态',
  `createDate` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `lastModifiedDate` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES (1, 'C#', '1', '2022-06-03 17:27:05', '2022-06-03 17:27:08');
INSERT INTO `category` VALUES (2, 'Linux', '1', '2022-06-03 17:38:40', '2022-06-03 17:38:43');
INSERT INTO `category` VALUES (3, 'Java', '1', '2022-07-17 21:31:15', '2022-07-17 21:31:15');
INSERT INTO `category` VALUES (4, 'Rust', '1', '2022-07-17 21:31:19', '2022-07-17 22:25:05');
INSERT INTO `category` VALUES (5, 'Python', '1', '2022-07-17 22:05:23', '2022-07-17 22:30:22');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `nickname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '昵称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'tingyu', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '听雨');

SET FOREIGN_KEY_CHECKS = 1;
