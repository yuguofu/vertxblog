package vertxblog;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import vertxblog.api.ApiVerticle;
import vertxblog.service.DataService;

public class MainVerticle extends AbstractVerticle {

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.exceptionHandler(err -> System.out.println(err.getMessage()));

        Future.<Void>succeededFuture()
            .compose(v -> Future.<String>future(s ->
                vertx.deployVerticle(new ApiVerticle(),
                    new DeploymentOptions()
                        .setConfig(config()), s)))
            .compose(v -> Future.<String>future(s ->
                vertx.deployVerticle(new DataService(),
                    new DeploymentOptions()
                        .setWorker(true)
                        .setConfig(config()), s)))
            .onComplete(res -> {
                if (res.succeeded()) {
                    startPromise.complete();
                } else {
                    startPromise.fail("Vert.x failed to start  " + res.cause().getMessage());
                }
            });


    }
}
